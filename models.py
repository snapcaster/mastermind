from dataclasses_json import dataclass_json
from dataclasses import dataclass

from typing import List
import os

@dataclass_json
@dataclass
class Agent:
    name: str
    rules: 'Ruleset'

    def guess(self, game: 'Game') -> List[int]:
        raise NotImplementedError('Agents must implement guess method')


@dataclass_json
@dataclass
class Ruleset:
    num_colors: int = 6
    num_slots: int = 4
    num_guesses: int = 10
    play_until_won: bool = False

@dataclass_json
@dataclass
class Response:
    red_pins: int
    white_pins: int
    empty_pins: int

@dataclass_json
@dataclass
class Game:
    code: List[int]
    guesses: List[List[int]]
    responses: List[Response]
    agent: Agent
    rules: Ruleset
    uuid: str


    def save(self, root_dir):
        # make dir
        os.makedirs(root_dir, exist_ok=True)
        save_path = os.path.join(root_dir, f'{self.uuid}.json')
        with open(save_path, 'w') as f:
            f.write(self.to_json())

