
from dataclasses_json import dataclass_json
from dataclasses import dataclass

import argparse

from datetime import datetime
from enum import Enum
from itertools import product
from typing import List, Callable
import os, sys
import random
import uuid

import getch

from graphics import print_code_colors, print_row, print_game, printProgressBar
from models import Agent, Ruleset, Game, Response
APPLICATION_DATA_DIR = os.path.join(os.environ['HOME'], '.local', 'share', 'mastermind')

GUESS_CLEAR_CHARACTER = 'q'

POSSIBLE_RESPONSE = [
[4, 0, 0], # right answer
[3, 1, 0], # [3, 0, 1] doesn't exist, since 3 posistion are right
[2, 2, 0],
[2, 1, 1],
[2, 0, 2],
[1, 3, 0],
[1, 2, 1],
[1, 1, 2],
[1, 0, 3],
[0, 4, 0],
[0, 3, 1],
[0, 2, 2],
[0, 1, 3],
[0, 0, 4]
]

def uid() -> str:
    return str(uuid.uuid4())


def permutations_with_replacement(n, m):
    # n: number of places
    # m: number of range
    for i in product(list(range(0, m)), repeat=n):
        yield i


@dataclass_json
@dataclass
class HumanAgent(Agent):

    def guess(self, game: 'Game') -> List[int]:
        print_code_colors()
        print_game(game, print_code=False)
        guess = []
        while len(guess) < game.rules.num_slots:
            c = getch.getch()
            if c == GUESS_CLEAR_CHARACTER:
                guess = []
            else:
                try:
                    color_index = int(c)
                    if color_index >= 0 and color_index < game.rules.num_colors:
                        guess.append(color_index)
                except ValueError:
                    continue
            print_row(guess, end='\r')
        print()
        return guess


def _norm(proba: List[float]) -> List[float]:
    mag = sum(proba)
    return [x/mag for x in proba]


class RandomAgent(Agent):


    def guess(self, game: 'Game') -> List[int]:
        return [ random.randint(0, game.rules.num_colors - 1) for i in range(game.rules.num_slots) ]


class BaselineAgent(Agent):

    def __init__(self, rules, name):
        self.name = name
        uniform_proba = 1.0 / rules.num_colors
        def jitter(magnitude: float) -> float:
            if random.random() <= 0.5:
                return -1 * random.random() * magnitude
            else:
                return random.random() * magnitude

        self.slot_probabilities = [[uniform_proba + jitter(0.002) for j in range(rules.num_colors)] for i in range(rules.num_slots)]
        self._normalize()

    def guess(self, game: 'Game') -> List[int]:
        self.update(game)
        return [slot_proba.index(max(slot_proba)) for slot_proba in self.slot_probabilities]

    def _normalize(self):
        


        self.slot_probabilities = [_norm(sp) for sp in self.slot_probabilities]

    def _update(self, game: 'Game'):
        priors = self.slot_probabilities
        probas = []


class MaxProbaAgent(Agent):

    def __init__(self, rules, name):
        self.name = name
        self.rules = rules
        permutations = []
        for i in permutations_with_replacement(rules.num_slots, rules.num_colors):
            permutations.append(list(i))

        self.all_candidates = permutations

        self.available_candiates = list(self.all_candidates)
        self.tried_candiates = None

    def _getAvailableCandidate(self, guess: List, response: Response):
        output = []

        for candidate in self.available_candiates:
            predict_response = get_response(guess, candidate)
            if predict_response == response:
                output.append(candidate)

        return output

    # didn't use this
    def _findNextCandidate(self):
        max_reduce_possibility = 0
        nextcandidate = None

        # search among all_candidates instead of available_candidates
        for candidate in self.all_candidates:
            if candidate not in self.tried_candiates:
                reducePossibilty = 0
                
                # assume we provide "candidate" as the next guess, we can get different response from POSSIBLE_RESPONSE
                # for each response, we can reduce certain amount of candidates
                # reducePossibilty = sum(reduction) for reponse in POSSIBLE_RESPONSE
                # we want to find the candidate that reduce the maximal possbility
                for response in POSSIBLE_RESPONSE:
                    reducePossibilty += len(self.available_candiates) - len(self._getAvailableCandidate(candidate, response))

                if reducePossibilty > max_reduce_possibility:
                    max_reduce_possibility = reducePossibilty
                    nextcandidate = candidate

        return max_reduce_possibility, nextcandidate

    def guess(self, game: 'Game') -> List[int]:
        # first guess, randome
        if len(game.responses) == 0:
            return [1, 1, 2, 2]
        else:
            last_response = game.responses[-1]
            last_guess = game.guesses[-1]
            self.tried_candiates = game.guesses

            # reduce the available_candidates based on last_guess, last_response
            available_candidate = self._getAvailableCandidate(last_guess, last_response)
            self.available_candiates = available_candidate
            return self.available_candiates[0]

            # find the next candidate that reduce the max possibities 
            # _, nextcandidate = self._findNextCandidate()
            # return nextcandidate


def get_response(guess: List[int], code: List[int]) -> Response:
    if len(guess) != len(code):
        raise ValueError(f'Guess is length {len(guess)} but code is length {len(code)}')

    num_red = 0
    num_white = 0
    used_indices = []
    for i in range(len(guess)):
        if guess[i] == code[i]:
            num_red += 1
            used_indices.append(i)

    for i in range(len(guess)):
        if guess[i] == code[i]:
            continue
        
        for j in range(len(guess)):
            if guess[i] == code[j] and j not in used_indices:
                num_white += 1
                used_indices.append(j)
                break
        
    return Response(red_pins=num_red, white_pins=num_white, empty_pins=len(guess) - (num_red + num_white))
    

def play_game(game: Game, render: bool = True) -> bool:
    while game.code not in game.guesses and len(game.responses) < game.rules.num_guesses:
        if render:
            os.system('clear')
        guess = game.agent.guess(game)
        response = get_response(guess, game.code)
        game.guesses.append(guess)
        game.responses.append(response)
    
    if render:
        print_game(game)

        print('Code was:\n')
        print_row(game.code)
        print()

    if game.code in game.guesses:
        if render:
            print('Congrats you won!')
        return True
    else:
        if render:
            print('You lost haha')
        return False

def run_simulation(num_games, rules: Ruleset, agent_factory: Callable[[], Agent], savedir: str, render: bool):
    wins = 0
    printProgressBar(0, num_games, prefix = 'Progress:', suffix = 'Complete', length = 80)
    start = datetime.now()
    for i in range(num_games):
        agent = agent_factory()
        g = init_game(agent, rules)
        won_game = play_game(g, render)
        if won_game:
            wins += 1
        g.save(savedir)
        printProgressBar(i+1, num_games, prefix = 'Progress:', suffix = 'Complete', length = 80)
    end = datetime.now()
    print()
    sim_seconds = (end - start).total_seconds()
    games_per_second = round(num_games/sim_seconds, 2)
    print(f'Simulation time: {sim_seconds} seconds or {games_per_second} games per second')
    print(f'Win Rate: {float(wins)/num_games}')


def random_code(code_length: int, code_colors: int) -> List[int]:
    return [ random.randint(0, code_colors - 1) for i in range(code_length) ]

def init_game(agent: Agent, rules: Ruleset) -> Game:
    code = random_code(rules.num_slots, rules.num_colors)
    return Game(code, [], [], agent, rules, uid())


agents = {
    'human': HumanAgent,
    'random': RandomAgent,
    'max_proba': MaxProbaAgent
}

def main() -> int:
    p = argparse.ArgumentParser()
    p.add_argument('--application-data-dir', type=str, default=APPLICATION_DATA_DIR, help='directory to store game and other data')
    p.add_argument('--num-colors', type=int, default=6, help='number of colors each slot is able to occupy')
    p.add_argument('--num-slots', type=int, default=4, help='number of pegs the code is comprised of')
    p.add_argument('--num-guesses', type=int, default=10, help='number of guesses allowed before the game is lost')
    p.add_argument('--num-games', type=int, default=1, help='number of games to play before exiting')
    p.add_argument('--agent', help='agent to use', choices=agents.keys(), default='human')
    p.add_argument('label', type=str, help='name to use to record the game statistics')
    args = p.parse_args()
    print('starting mastermind simulator...')

    rules = Ruleset(
        num_colors = args.num_colors,
        num_slots = args.num_slots,
        num_guesses = args.num_guesses
    )



    def agent_factory():
        if args.agent not in agents:
            raise NotImplementedError(f'No implementation defined for {args.agent}')
        return agents[args.agent](rules=rules, name=args.label)
        
    run_simulation(args.num_games, rules, agent_factory, args.application_data_dir, args.agent == 'human')


if __name__ == '__main__':
    sys.exit(main())