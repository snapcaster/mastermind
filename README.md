# Mastermind Simulator

Simple command line program to enable playing Mastermind and running simulations with programmed agents

# Usage

To install

`pipenv install`

To run

`pipenv run python mastermind.py`

By default runs in interactive mode. If you want to create your own agent just check out `mastermind.py` and subclass `Agent` and define the `guess` method and you can integrate it easily into the `--agent` argument on command line